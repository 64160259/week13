package com.panisa.swingtutorial;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueueList, lblCurrent;
    JButton btnAddQueue, btnGetQueue, btnClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        queue = new LinkedList();

        this.setSize(400, 300);
        txtName = new JTextField();
        txtName.setBounds(30, 10, 200, 20);
        txtName.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
                
            }});

        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(250, 10, 100, 20);
        btnAddQueue.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
                
            }});

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250, 40, 100, 20);
        btnGetQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
                
            }});

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250, 70, 100, 20);
        btnClearQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
                
            }});

        lblQueueList = new JLabel("Empty");
        lblQueueList.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(txtName);
        this.add(btnAddQueue);
        this.add(btnGetQueue);
        this.add(btnClearQueue);
        this.add(lblQueueList);
        this.add(lblCurrent);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showQueue();
        this.setVisible(true);
    }

    public void showQueue() {
        if(queue.isEmpty()) {
            lblQueueList.setText("Empty");
        } else {
            lblQueueList.setText(queue.toString());
        }
    }

    public void getQueue() {
        if(queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void addQueue() {
        String name = txtName.getText();
        if(name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }

    public void clearQueue() {
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}
