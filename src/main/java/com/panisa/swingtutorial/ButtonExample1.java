package com.panisa.swingtutorial;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

class MyFrame1 extends JFrame {
    JButton button;
    JButton clearButton;
    JTextField text;
    public MyFrame1() {
        super("Button Example");
        text = new JTextField();
        text.setBounds(50, 50, 150, 20);
        button = new JButton("Welcome");
        button.setBounds(50, 100, 110, 30);
        button.setIcon(new ImageIcon("waving-hand.png"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button");
                text.setText("Welcome to Burapha");
            }});
        clearButton = new JButton("Clear");
        clearButton.setBounds(165, 100, 95, 30);
        clearButton.setIcon(new ImageIcon("clear.png"));
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                
            }});
        this.add(text);
        this.add(button);
        this.add(clearButton);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
public class ButtonExample1 {
    public static void main(String[] args) {
        MyFrame1 frame = new MyFrame1();
    }
}
